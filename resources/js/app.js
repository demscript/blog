// require('./bootstrap');
import Vue from 'vue'
import { createInertiaApp, InertiaLink } from '@inertiajs/inertia-vue'
import { InertiaProgress } from '@inertiajs/progress'
import { Link } from '@inertiajs/inertia-vue'
import BootstrapVue from "bootstrap-vue";
import VueElementLoading from "vue-element-loading";
import CKEditor from '@ckeditor/ckeditor5-vue2';
import axios from "axios";
import Toasted from 'vue-toasted';
import VueMask from 'v-mask';
Vue.use(CKEditor);
Vue.component('pagination', require('laravel-vue-pagination'));
Vue.use(VueMask);
Vue.component('inertia-link', InertiaLink)
// Vue.use(plugin)
Vue.use(Toasted)
Vue.use(BootstrapVue);
Vue.use(axios);
Vue.use(VueElementLoading);
Vue.use(Link);
createInertiaApp({
    resolve: name => require(`./Pages/${name}`),
    setup({ el, App, props }) {
        new Vue({
            render: h => h(App, props),

        }).$mount(el)
    },
})


InertiaProgress.init({
    // The delay after which the progress bar will
    // appear during navigation, in milliseconds.
    delay: 250,

    // The color of the progress bar.
    color: 'yellow',

    // Whether to include the default NProgress styles.
    includeCSS: true,

    // Whether the NProgress spinner will be shown.
    showSpinner: true,
})

Vue.mixin({
    methods: {
        toaster(message, status) {
            Vue.toasted.show(message, {
                type: status,
            }).goAway(4000)
        }
    }
});
