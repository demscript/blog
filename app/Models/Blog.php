<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Blog extends Model
{
    use HasFactory;
    protected $fillable = [
        'content',
        'title',
        'image',
    ];
    protected $casts = [
        'id' => 'string'
    ];

    public static function boot()
    {
        parent::boot();
        static::creating(function ($instance) {
            $instance->id = Str::uuid();
        });
    }
    public function comment()
    {
        return $this->hasMany(Comment::class);
    }
}
