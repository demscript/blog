<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Comment extends Model
{
    use HasFactory;
    protected $fillable = [
        'comments',
        'blog_id',
    ];

    public static function boot()
    {
        parent::boot();
        static::creating(function ($instance) {
            $instance->id = Str::uuid();
        });
    }

    public function blog()
    {
        return $this->belongsTo(Blog::class);
    }
}
