<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Inertia\Inertia;
use App\Models\Blog;
use App\Models\Comment;

class WebsiteController extends Controller
{
    public function home()
    {
        $blog = Blog::with("comment")->limit(3)->get();
        return Inertia::render('Authenticated/User/Index/Index', [
            'blogs' => $blog,
        ]);
    }
    public function showBlog($id)
    {
        $blog = Blog::where('id', '=', $id)->with(['comment'])->first();
        $comment = Comment::where('blog_id', '=', $id)->get();
        $commentNum = count($comment);
        return Inertia::render('Authenticated/User/Blog/Index', [
            'blog' => $blog,
            'comments' => $comment,
            'commentNum' => $commentNum,
        ]);
    }

    public function getComments(Request $request)
    {
        return Comment::where('blog_id', '=', $request->id)->get();
    }
}
