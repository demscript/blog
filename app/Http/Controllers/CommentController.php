<?php

namespace App\Http\Controllers;

use App\Models\Comment;
use Illuminate\Http\Request;
use Inertia\Inertia;

class CommentController extends Controller
{
    public function index()
    {
        return Inertia::render('Authenticated/Admin/Comment/Index');
    }
    public function addComment(Request $request)
    {
        $validated = request()->validate([
            'comment' => 'required',
            'name' => 'required',
        ]);
        if ($validated) {
            $comment = new Comment;
            $comment->blog_id = request()->id;
            $comment->comment = request()->comment;
            $comment->name = request()->name;
            $save = $comment->save();
            if ($save) {
                return "Comment Sent";
            }
        }
    }
    public function getComments()
    {
        return Comment::with(['blog'])->paginate(10);
    }
}
