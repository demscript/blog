<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Inertia\Inertia;
use App\Models\Blog;

class BlogController extends Controller
{
    public function index()
    {
        return Inertia::render('Authenticated/Admin/Blog/Index');
    }
    public function addBlogs(Request $request)
    {
        $validated = $request->validate([
            'title' => 'required|unique:blogs',
            'content' => 'required',
            'image' => 'required',
        ]);
        if ($validated) {
            // dd(request()->file('image'));
            $image = time() . '_' . request()->file('image')->getClientOriginalName();
            request()->file('image')->storeAs('public/assets/blogs/images', $image);
            $blog = new Blog;
            $blog->title = $request->title;
            $blog->image = $request->title;
            $blog->content = $request->content;
            $save = $blog->save();

            if ($save) {
                return response()->json(['success' => 'Blog Added']);
            }
        }
    }



    public function getBlogs(Request $request)
    {
        $data = Blog::paginate(10);
        if ($data) {
            return $data;
        }
    }

    // To Search Blogs

    public function search(Request $request)
    {
        // dd($request->name);
        return
            Blog::where('title', 'like', '%' . $request->name . '%')->limit(5)->get();
    }
}
