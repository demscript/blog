<?php

namespace App\Http\Middleware;

use App\Providers\RouteServiceProvider;
use Closure;
use Illuminate\Http\Request;
use Inertia\Inertia;

class RedirectIfProfileIsIncomplete
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */

    public function handle(Request $request, Closure $next)
    {
        $user = auth()->user();
        $checkUser = [
            $user->surname, $user->firstname, $user->middlename, $user->email, $user->phone, $user->gender, $user->account_name,
            $user->bank_name, $user->account_number, $user->address, $user->marital_status, $user->dob
        ];
        if (count(array_filter($checkUser)) == sizeof($checkUser)) {
            return $next($request);
        }

        return redirect('/profile');
    }
}
