<?php

use Illuminate\Support\Facades\Route;
use Inertia\Inertia;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();
Route::middleware(['guest'])->group(function () {
    Route::get('/register', function () {
        return Inertia::render('Auth/Register');
    })->name('register');
    Route::get('/login', function () {
        return Inertia::render('Auth/Login');
    })->name('login');
});

// Routes For Website
Route::get('/blog/{id}', [App\Http\Controllers\WebsiteController::class, 'showBlog']);
Route::get('/home', [App\Http\Controllers\WebsiteController::class, 'home']);
Route::post('/blog/addComment', [App\Http\Controllers\CommentController::class, 'addComment']);
Route::post('/blog/getComments', [App\Http\Controllers\WebsiteController::class, 'getComments']);
Route::post('/blog/search-blog', [App\Http\Controllers\BlogController::class, 'search'])->name('search');


Route::middleware(['auth'])->group(
    function () {
        Route::get('/', function () {
            return Inertia::render('Authenticated/Index');
        });
        Route::get('/manage-blog', [App\Http\Controllers\BlogController::class, 'index']);
        Route::get('/manage-comment', [App\Http\Controllers\CommentController::class, 'index']);
        Route::get('/get-comments', [App\Http\Controllers\CommentController::class, 'getComments']);
        Route::post('/addBlogs', [App\Http\Controllers\BlogController::class, 'addBlogs']);
        Route::get('/getBlogs', [App\Http\Controllers\BlogController::class, 'getBlogs']);
    }
);
